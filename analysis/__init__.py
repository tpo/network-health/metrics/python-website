import inspect
from collections import OrderedDict
from collections import namedtuple

import analysis.onionservices
import analysis.performance
import analysis.servers
import analysis.summary
import analysis.traffic
import analysis.users
import analysis.maps

from analysis.utils import analysis_title, analysis_description


MetricsAnalysis = namedtuple("MetricsAnalysis", ["name", "type"])


analysis_modules = [  # Order matters
    analysis.servers,
    analysis.traffic,
    analysis.users,
    analysis.performance,
    analysis.onionservices,
    analysis.summary,
    analysis.maps

]

plots = dict()
tables = dict()
categories = OrderedDict()

for module in analysis_modules:
    module_title = analysis_title(module)
    categories[module_title] = {
        "description": analysis_description(module),
        "module": module,
        "metrics": []
    }
    for member in inspect.getmembers(module):
        if member[0].startswith("plot_"):
            name = member[0][len("plot_"):]
            func = member[1]
            plots[name] = func
            categories[module_title]["metrics"].append(
                MetricsAnalysis(name, "plot")
            )
        if member[0].startswith("table_"):
            name = member[0][len("table_"):]
            func = member[1]
            tables[name] = func
            categories[module_title]["metrics"].append(
                MetricsAnalysis(name, "table")
            )

# We don't want the summary plot to show up in the nav
categories.pop("Summary")
