from collections import OrderedDict
import json

choices = dict()
filters = ["countries", "percentiles", "flags", "transports", "filesize", "webserver"]

for f in filters:
    with open(f"analysis/data/{f}.json") as filter_f:
        choices[f] = json.load(filter_f, object_pairs_hook=OrderedDict)

choices["relay_type"] = {"relay": "Relay", "bridge": "Bridge"}
