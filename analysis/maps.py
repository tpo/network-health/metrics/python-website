"""
Maps.
"""
import datetime

try:
    import geopandas
except OSError:
    geopandas = None

import requests
import pandas as pd
from matplotlib.colors import LinearSegmentedColormap
from analysis.utils import add_missing_countries, read_in_file
from matplotlib.figure import Figure
import seaborn as sns

sns.set_theme(context="notebook")

cdf = pd.read_csv("analysis/data/countries.csv")
cdf_dict = dict(zip(cdf["alpha-2"], cdf["alpha-3"]))

tor_colors = ["#F8F9FA", "#7D4698", "#59316B"]
tor_cmap = LinearSegmentedColormap.from_list("mycmap", tor_colors)


def plot_relay_map():
    """
    Relays by Country.

    Map of relays by country.
    """
    if geopandas is None:
        return
    relays = requests.get("https://onionoo.torproject.org/details?type=relay&fields=country")
    r = relays.json()["relays"]
    relay_df = pd.DataFrame.from_dict(r)
    relay_df = relay_df[relay_df.country != "eu"]
    relay_df["alpha_3"] = relay_df.country.apply(lambda x: cdf_dict[x.upper()])

    relay_df = relay_df.groupby("alpha_3").count()
    relay_df["iso_a3"] = relay_df.index
    relay_df = add_missing_countries(relay_df)
    world = geopandas.read_file(geopandas.datasets.get_path('naturalearth_lowres'))
    world = world[(world.name != "Antarctica") & (world.name != "Fr. S. Antarctic Lands")]
    w = world.merge(relay_df, how="right")
    fig = Figure(figsize=(12, 7))
    ax = fig.subplots()
    w.plot(column='country',
           ax=ax,
           legend=True,
           legend_kwds={'orientation': "horizontal"},
           missing_kwds={"color": "white",
                         "edgecolor": "darkgrey",
                         "hatch": "///",
                         "label": "Missing values", },
           edgecolor="darkgrey",

           cmap=tor_cmap)
    ax.set_title(
        f"Relays by Country - {relays.json()['relays_published']}")
    return fig


def plot_users_map(relay_type):
    """
    Users by Country.

    Map of users by country.
    """
    if geopandas is None:
        return
    pin = read_in_file("clients.csv")
    pin = (pin[(pin.date == pin.date.max() - datetime.timedelta(days=1))
               & (pin.country.notna())
               & (~pin.country.isin(["xk", "??", "an", "na", "ap", "eu", "cs"]))])
    for n in [relay_type]:
        p = pin[pin.node == n].copy()
        p["alpha_3"] = p["country"].apply(lambda x: cdf_dict[x.upper()])
        p = p.groupby("alpha_3").sum()
        p["iso_a3"] = p.index
        p = add_missing_countries(p)

        world = geopandas.read_file(geopandas.datasets.get_path('naturalearth_lowres'))
        world = world[(world.name != "Antarctica") & (world.name != "Fr. S. Antarctic Lands")]
        w = world.merge(p, how="right")
        fig = Figure(figsize=(12, 7))
        ax = fig.subplots()

        w.plot(column='clients',
               ax=ax,
               legend=True,
               legend_kwds={'orientation': "horizontal"},
               missing_kwds={"color": "white",
                             "edgecolor": "lightgrey",
                             "hatch": "///",
                             "label": "Missing values", },
               **{"edgecolor": "darkgrey", },
               cmap=tor_cmap)
        ax.set_title(f"{n.capitalize()} Users by Country - {(pin.date.max() - datetime.timedelta(days=1)).strftime('%Y-%m-%d')}")
    return fig
