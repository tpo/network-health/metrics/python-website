"""
Onion Services.

Onion services are services that are only accessible via the Tor network.
"""

from matplotlib.figure import Figure

import seaborn as sns
from analysis.utils import prepare_file, set_labels

sns.set_theme(context="notebook")

def plot_hidserv_dir_onions_seen(start, end):
    """
    Unique .onion addresses.

    This graph shows the number of unique .onion addresses for version 2 onion services in the network per day. These numbers are extrapolated from aggregated statistics on unique version 2 .onion addresses reported by single relays acting as onion-service directories, if at least 1% of relays reported these statistics.

    :param start:
    :param end:
    :return: the rendered plot
    """
    p = prepare_file(start, end, "hidserv.csv")
    p = p[(p.type == "dir-onions-seen") & (p.frac > 0.01)]
    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()

    ax.plot(p.date, p.wiqm)
    set_labels(ax, "Number of unique .onion addresses", "Date", "Total")
    return fig


def plot_hidserv_dir_v3_onions_seen(start, end):
    """
    Unique .onion v3 addresses.

    This graph shows the number of unique .onion addresses for version 3 onion services in the network per day. These numbers are extrapolated from aggregated statistics on unique version 3 .onion addresses reported by single relays acting as onion-service directories, if at least 1% of relays reported these statistics.

    :param start:
    :param end:
    :return: the rendered plot
    """
    p = prepare_file(start, end, "v3hidserv.csv")
    p = p[(p.type == "dir-onions-seen") & (p.frac > 0.01)]
    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()

    ax.plot(p.date, p.wiqm)
    set_labels(ax, "Number of unique .onion v3 addresses", "Date", "Total")

    return fig

def plot_hidserv_rend_relayed_cells(start, end):
    """
    Onion service traffic

    This graph shows the amount of onion-service traffic from version 2 and version 3 onion services relayed by rendezvous points. This number is extrapolated from aggregated statistics on onion-service traffic reported by single relays acting as rendezvous points for version 2 and 3 onion services, if at least 1% of relays reported these statistics.

    :param start:
    :param end:
    :return: the rendered plot
    """
    p = prepare_file(start, end, "hidserv.csv")
    p = p[(p.type == "rend-relayed-cells") & (p.frac > 0.01)]
    p.wiqm = p.wiqm.apply(lambda x: x * 8 * 512 / (86400 * 1e9))
    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()
    ax.plot(p.date, p.wiqm)
    set_labels(ax, "Onion service traffic", "Date", "Gbit/s")

    return fig