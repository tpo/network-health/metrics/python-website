"""
Performance.

We use Torperf and OnionPerf to run performance measurements. Both work by fetching files of different sizes over Tor
and measuring how long that takes.
"""

from matplotlib.figure import Figure
from matplotlib.ticker import FormatStrFormatter
from matplotlib import rcParams

import seaborn as sns
from analysis.utils import prepare_file, set_labels

sns.set_theme(context="notebook")


def plot_onionperf_throughput(start, end, webserver):
    """
    Throughput.

    This graph shows throughput when downloading static files of different sizes over
    Tor, either from a server on the public internet or from an onion server.
    Throughput is calculated from the time between receiving 4 and 5 MiB of the
    response. The graph shows the median of measurements as thick line, the range
    of measurements from first to third quartile as ribbon, and the highest and lowest
    non-outlier measurements as thin lines.

    :param start: start date
    :param end: end date
    :param webserver: type of measurement, onion or public
    :return: the rendered plot
    """
    p = prepare_file(start, end, "onionperf-throughput.csv")
    p = p[(p.server == webserver) & (p.source.notna())]
    subplots = len(p.source.unique())
    fig = Figure(figsize=(12, subplots * 2))
    axes = fig.subplots(subplots, 1)
    count = 0
    colors = iter(rcParams['axes.prop_cycle'] * subplots)
    for source in p.source.unique():
        col = next(colors)['color']
        p1 = p[p.source == source]
        axes[count].plot(p1.date, p1.md, label=source, color=col)
        axes[count].plot(p1.date, p1.high, color=col)
        axes[count].plot(p1.date, p1.low, color=col)

        axes[count].fill_between(p1.date, p1.q1, p1.md, alpha=0.4, color=col)
        axes[count].fill_between(p1.date, p1.md, p1.q3, alpha=0.4, color=col)
        axes[count].legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0)
        axes[count].yaxis.set_major_formatter(FormatStrFormatter('%.0f ms'))

        count += 1

    set_labels(axes[0], f"Throughput when dowloading from {webserver} server", "", "", add_copyright_notice=False)
    set_labels(axes[subplots - 1], "", "Date", "", add_copyright_notice=False)
    return fig

def plot_onionperf_latencies(start, end, webserver):
    """
     Circuit round trip latencies.

    This graph shows round-trip latencies of circuits used for downloading static
    files of different sizes over Tor, either from a server on the public internet
    or from an onion server. Round-trip latencies are measured as the time between
    sending the HTTP request and receiving the HTTP response header.
    The graph shows the median of measurements as thick line, the range of
    measurements from first to third quartile as ribbon, and the highest and lowest
    non-outlier measurements as thin lines.

    :param start: start date
    :param end: end date
    :param webserver: type of measurement, onion or public
    :return: the rendered plot
    """
    p = prepare_file(start, end, "latencies.csv")
    p = p[(p.server == webserver) & (p.source.notna())]
    subplots = len(p.source.unique())
    fig = Figure(figsize=(12, subplots*2))
    axes = fig.subplots(subplots, 1)
    count = 0
    colors = iter(rcParams['axes.prop_cycle'] *subplots)
    for source in p.source.unique():
        col = next(colors)['color']
        p1 = p[p.source == source]
        axes[count].plot(p1.date, p1.md, label=source, color=col)
        axes[count].plot(p1.date, p1.high, color=col)
        axes[count].plot(p1.date, p1.low, color=col)

        axes[count].fill_between(p1.date, p1.q1, p1.md, alpha=0.4, color=col)
        axes[count].fill_between(p1.date, p1.md, p1.q3, alpha=0.4, color=col)
        axes[count].legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0)
        axes[count].yaxis.set_major_formatter(FormatStrFormatter('%.0f ms'))
        count += 1

    set_labels(axes[0], f"Round trip latencies from {webserver} server", "", "", add_copyright_notice=False)
    set_labels(axes[subplots - 1], "", "Date", "", add_copyright_notice=False)
    return fig

def plot_onionperf_buildtimes(start, end):
    """
    Circuit build times.

    This graph shows build times of circuits used for downloading static
    files of different sizes over Tor. We disable Entry Guards in the Tor client
    configuration used by OnionPerf, to ensure a new guard is selected for each
    circuit we measure. In comparison, standard clients will reuse connections to a small
    number of long-term entry guards, and do not see the latency introduced by
    establishing new guard connections.
    As a result, first hop build times appear higher for these measurements compared to
    those commonly observed in Tor clients.
    The graph shows the range of measurements from first to third quartile,
    and highlights the median.
    The slowest and fastest quarter of measurements are omitted from the graph.

    :param start: start date
    :param end: end date
    :return: the rendered plot
    """
    #axes2 = {}

    p = prepare_file(start, end, "buildtimes.csv")
    p = p[p.source.notna()]
    fig = Figure(figsize=(12, 6))
    axes = fig.subplots(3,1)
    for hop in p.position.unique():
        p1 = p[p.position == hop]
        for source in p1.source.unique():
            p2 = p1[p1.source == source]
            plot = axes[hop-1].plot(p2.date, p2.md, label=source)
            axes[hop-1].fill_between( p2.date, p2.q1, p2.md, alpha=0.4, color=plot[-1].get_color())
            axes[hop-1].fill_between( p2.date, p2.md, p2.q3, color= plot[-1].get_color(), alpha=0.3)
            axes[hop-1].yaxis.set_major_formatter(FormatStrFormatter('%.0fms'))
            #axes2[hop-1] = axes[hop-1].twinx()
            #axes2[hop-1].yaxis.set_ticks_position('none')
            #axes2[hop-1].yaxis.set_major_formatter(FormatStrFormatter(''))

    set_labels(axes[0], "Circuit build-times" , "", "1st Hop", add_copyright_notice=False)
    set_labels(axes[1], "", "", "2nd Hop", add_copyright_notice=False)
    set_labels(axes[2], "", "Date", "3rd Hop", add_copyright_notice=False)

    axes[1].legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0)
    return fig

def plot_torperf(start, end, webserver, filesize):
    """
    Time to download files over Tor.

    This graph shows overall performance when downloading static files of different
    sizes over Tor, either from a server on the public internet or from an onion
    server.
    Download times include complete downloads of the shown file size as well
    as partial downloads of larger file sizes. The graph shows the range of measurements
    from first to third quartile, and highlights the median.
    The slowest and fastest quarter of measurements are omitted from the graph.

    :param start: start date
    :param end: end date
    :param webserver: type of measurement, onion or public
    :param filesize: file size in bytes
    :return: the rendered plot
    """
    filesize = int(filesize)
    filesize_readable = {51200: "50KiB",
                         1048576: "1MiB",
                         5242880: "5MiB"}
    p = prepare_file(start, end,"onionperf-including-partials.csv")
    p = p[(p.server == webserver) & (p.filesize == filesize)]
    p.md = p.md.apply(lambda x: x/1000)
    p.q3 = p.q3.apply(lambda x: x/1000)
    p.q1 = p.q1.apply(lambda x: x/1000)

    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()
    instances = p.source.unique()

    for instance in instances:
        data = p[p.source == instance]
        plot = ax.plot(data.date, data.md, label=instance)
        ax.fill_between( data.date, data.q1, data.md, alpha=0.3, color=plot[-1].get_color())
        ax.fill_between( data.date, data.md, data.q3, color= plot[-1].get_color(), alpha=0.3)
    set_labels(ax, f"Time to download {filesize_readable[filesize]} from {webserver} server", "Date", "Time to download (seconds)")
    ax.legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0)
    return fig

def plot_torperf_failures(start, end, webserver):
    """
    Timeouts and failures of downloading over Tor.

    This graph shows the fraction of timeouts and failures when downloading
    static files of different sizes over Tor, either from a server on the
    public internet or from an onion server.
    A timeout occurs when a download does not complete within the scheduled time,
    in which case it is aborted in order not to overlap with the next scheduled
    download.
    A failure occurs when the download completes, but the response is smaller than expected.

     :param start: start date
     :param end: end date
     :param webserver: type of measurement, onion or public
     :return: the rendered plot
     """
    p = prepare_file(start, end, "onionperf-failures.csv")
    p = p[p.server == webserver]
    p.timeouts = p.timeouts/p.requests
    p.failures = p.failures/p.requests

    fig = Figure(figsize=(12, 6))
    axes = fig.subplots(2,1)
    axes[0] = sns.scatterplot(data=p, x="date", y="timeouts", hue="source", ax=axes[0])
    l =  axes[0].get_ylim()
    axes[1] = sns.scatterplot(data=p, x="date", y="failures", hue="source", ax=axes[1])
    axes[1].set_ylim(l)

    set_labels(axes[0], f"Timeouts and failures of requests to {webserver} server", "", "Timeouts %", add_copyright_notice=False)
    set_labels(axes[1], "", "Date", "Failures %",  push=-0.15)
    return fig