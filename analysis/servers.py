"""
Servers.

Country and ISP diversity are approximated by resolving IP addresses to a country code and an autonomous system.
We process the capabilities and properties relays and bridges reported to directory authorities.
"""

from matplotlib.figure import Figure
import seaborn as sns

from analysis.utils import prepare_file, add_missing_dates, set_labels

sns.set_theme(context="notebook")


def plot_networksize(start, end):
    """
    Relays and bridges.

    This graph shows the number of running relays and bridges in the network.

    :param start: start date
    :param end: end date
    :return: the rendered plot
    """
    p = prepare_file(start, end, "networksize.csv")
    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()
    sns.lineplot(data=p, ax=ax, x="date", y="relays", label="Relays")
    sns.lineplot(data=p, ax=ax, x="date", y="bridges", label="Bridges")
    set_labels(ax, "Number of relays", "Date", "Total")
    return fig


def plot_versions(start, end):
    """
    Relays by tor version.

    This graph shows the number of running relays by tor software version.
    Relays report their tor software version when they announce themselves
    in the network.  New major versions are added to the graph as soon as they
    are first recommended by the directory authorities. More details on when
    these versions were declared stable or unstable can be found on the
    `download page <https://www.torproject.org/download/download.html>`_
    and in the `changes file
    <https://gitlab.torproject.org/tpo/core/tor/-/raw/main/ChangeLog>`_.

    :param start: start date
    :param end: end date
    :return: the rendered plot
    """
    p = prepare_file(start, end, "versions.csv")
    fig = Figure(figsize=(12, 7))
    ax = fig.subplots()
    sns.lineplot(data=p, ax=ax, x="date", y="relays", hue="version")
    set_labels(ax, "Number of Tor relays by version", "Date", "Total")
    ax.legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0)
    return fig


def plot_platforms(start, end):
    """
    Relays by platform.

    This graph shows the number of running relays by operating system. Relays
    report their operating system when they announce themselves in the
    network.

    :param start: start date
    :param end: end date
    :return: the rendered plot
    """
    p = prepare_file(start, end, "platforms.csv")
    p["platform"] = p.platform.apply(lambda x: x.lower())
    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()
    sns.lineplot(data=p, ax=ax, x="date", y="relays", hue="platform")
    set_labels(ax, "Number of Tor relays by platform", "Date", "Total")
    return fig


def plot_relayflags(start, end):
    """
    Relays by relay flag.

    This graph shows the number of running relays that have had certain flags
    assigned by the directory authorities. These flags indicate that a relay
    should be preferred for either guard ("Guard") or exit positions ("Exit"),
    that a relay is suitable for high-bandwidth ("Fast") or long-lived
    circuits ("Stable"), or that a relay is considered a onion service
    directory ("HSDir").

    :param start: start date
    :param end: end date
    :return: the rendered plot
    """
    p = prepare_file(start, end, "relayflags.csv")
    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()
    sns.lineplot(data=p, ax=ax, x="date", y="relays", hue="flag")
    ax.set_xlabel("Date")
    ax.set_ylabel("Total")
    set_labels(ax, "Number of relays with relay flags assigned", "Date", "Total")
    return fig


def plot_relays_ipv6(start, end, relay_type="relay"):
    """
    Relays by IP version.

    This graph shows the number of relays supporting IPv6 as compared to all
    relays. A relay can support IPv6 by announcing an IPv6 address and port
    for the OR protocol, which may then be confirmed as reachable by the
    directory authorities, and by permitting exiting to IPv6 targets. These
    sets are not distinct, because relays can have various combinations of
    announced/confirmed OR ports and exit policies.

    :param start: start date
    :param end: end date
    :param relay_type: TODO
    :return: the rendered plot
    """
    p = prepare_file(start, end, "ipv6servers.csv")
    p = p[p["server"] == relay_type]
    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()
    sns.lineplot(data=p.groupby("valid_after_date").sum(), ax=ax, x="valid_after_date", y="server_count_sum_avg",
                 label="Total IPv4 (OR)")
    sns.lineplot(data=p[p["reachable_ipv6_relay"] == "t"].groupby("valid_after_date").sum(),
                 ax=ax,
                 x="valid_after_date",
                 y="server_count_sum_avg",
                 label="IPv6 reachable OR")
    sns.lineplot(data=p[p["exiting_ipv6_relay"] == "t"].groupby("valid_after_date").sum(),
                 ax=ax,
                 x="valid_after_date",
                 y="server_count_sum_avg",
                 label="IPv6 Exiting")
    sns.lineplot(data=p[p["announced_ipv6"] == "t"].groupby("valid_after_date").sum(),
                 ax=ax,
                 x="valid_after_date",
                 y="server_count_sum_avg",
                 label="IPv6 announced OR")

    set_labels(ax, "Relays by IP version", "Date", "Total")
    return fig


def plot_totalcw(start, end):
    """
    Total consensus weights across bandwidth authorities.

    This graph shows total consensus weights across bandwidth authorities.
    This graph may be useful for comparing bandwidth authority
    implementations by comparing their total bandwidth weights.

    :param start: start date
    :param end: end date
    :return: the rendered plot
    """
    p = prepare_file(start, end, "totalcw.csv")
    p = p.dropna()
    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()
    auths = p.nickname.unique()
    for auth in auths:
        df = p[p.nickname == auth]
        df = df.groupby("valid_after_date").sum()
        df["date"] = df.index
        df = add_missing_dates(df)
        ax.plot(df.date, df.measured_sum_avg, label=auth)
    set_labels(ax, "Total consensus weights across bandwidth authorities", "Date", "Total")
    return fig


