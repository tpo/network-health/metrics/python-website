"""
Summary.

Mix and match datasets on a single plot to explore trends in the Tor network.
"""

import inspect

from matplotlib.figure import Figure
import numpy as np
import analysis.summary_plotters
import analysis.trends
from analysis.utils import prepare_file

trends = dict()

for member in inspect.getmembers(analysis.trends):
    if member[0].startswith("trends_"):
        trends[member[0][7:]] = member[1]

plotters = dict()

for member in inspect.getmembers(analysis.summary_plotters):
    if member[0].startswith("summary_"):
        plotters[member[0][8:]] = member[1]

datasets = {
    "relaysize": "networksize.csv",
    "bridgesize": "networksize.csv",
    "advbandwidth_allrelays": "advbwdist.csv",
    "advbandwidth_exits": "advbwdist.csv",
    "exitsize": "relayflags.csv",
    "bridgeusers": "clients.csv",
    "relayusers": "clients.csv",
    "totalusers": "clients.csv",
    "cbandwidth_exits": "bandwidth.csv",
    "cbandwidth_guards": "bandwidth.csv",
    "cbandwidth_middle": "bandwidth.csv",
    "ttdl_5mb_public": "onionperf-including-partials.csv",
    "ttdl_5mb_onion": "onionperf-including-partials.csv",
    "onion_traffic": "hidserv.csv",
    "onion_v3_size": "v3hidserv.csv"

}


def plot_summary(start, end, dataset1="ttdl_5mb_onion", dataset2="totalusers"):
    """
    Tor Metrics Summary.

    This graph shows a summary of up to two Tor Metrics datasets.

    :param start: start date
    :param end: end date
    :param dataset1: dataset identifier
    :param dataset2: dataset identifier
    :return: the rendered plot
    """
    data1 = prepare_file(start, end, datasets[dataset1])
    data2 = prepare_file(start, end, datasets[dataset2])
    fig = Figure(figsize=(12, 6))
    ax1 = fig.add_subplot()
    ax2 = ax1.twinx()
    plotters[dataset1](ax1, data1, color="blue")
    plotters[dataset2](ax2, data2, color="orange")

    ax1.set_yticks([x for x in np.linspace(ax1.get_ybound()[0], ax1.get_ybound()[1], 10)])
    ax2.set_yticks([x for x in np.linspace(ax2.get_ybound()[0], ax2.get_ybound()[1], 10)])

    ax2.grid(False)
    ax1.legend(loc="upper left")
    ax2.legend(loc=0)
    return fig
