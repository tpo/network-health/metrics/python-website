from matplotlib.ticker import FormatStrFormatter
from analysis.utils import format_users, format_bandwidth


def summary_relaysize(ax, data, color):
    """
    Number of relays.
    """
    ax.plot(data["date"], data["relays"], color=color, label="Relays")
    ax.set_xlabel("Date")
    ax.set_ylabel("Number of relays")
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))


def summary_bridgesize(ax, data, color):
    """
    Number of bridges.
    """
    ax.plot(data["date"], data["bridges"], color=color, label="Bridges")
    ax.set_xlabel("Date")
    ax.set_ylabel("Number of bridges")
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))


def summary_exitsize(ax, data, color):
    """
    Number of exit relays.
    """
    data = data[data.flag == "Exit"]
    ax.plot(data["date"], data["relays"], color=color, label="Exit Relays")
    ax.set_xlabel("Date")
    ax.set_ylabel("Number of Exit relays")
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))


def summary_advbandwidth_allrelays(ax, data, color):
    """
    Advertised bandwidth (non-exit relays).
    """
    data["advbw"] = data.advbw.apply(lambda x: x*8/1e9)
    data = data[(data.percentile == 100) & (data.isexit != "t")]
    ax.plot(data["date"], data["advbw"], color=color, label="Total Bandwidth")
    ax.set_xlabel("Date")
    ax.set_ylabel("Advertised Relay Bandwidth")
    ax.yaxis.set_major_formatter(format_bandwidth)

def summary_advbandwidth_exits(ax, data, color):
    """
    Advertised bandwidth (exit relays).
    """
    data["advbw"] = data.advbw.apply(lambda x: x * 8 / 1e9)
    data = data[(data.percentile == 100) & (data.isexit == "t")]
    ax.plot(data["date"], data["advbw"], color=color, label="Exit Bandwidth")
    ax.set_xlabel("Date")
    ax.set_ylabel("Advertised Exit Bandwidth")
    ax.yaxis.set_major_formatter(format_bandwidth)


def summary_cbandwidth_guards(ax, data, color):
    """
    Consumed bandwidth (guard relays).
    """
    data["bwhist"] = data.bwread.apply(lambda x: x * 8 / 2e9) + data.bwwrite.apply(lambda x: x * 8 / 2e9)
    data["flavour"] = data.isexit + data.isguard
    data = data[data["flavour"] == "ft"]
    ax.plot(data["date"], data["bwhist"], color=color, label="Guard Bandwidth")
    ax.set_xlabel("Date")
    ax.set_ylabel("Consumed Guards Bandwidth")
    ax.yaxis.set_major_formatter(format_bandwidth)


def summary_cbandwidth_exits(ax, data, color):
    """
    Consumed bandwidth (exit relays).
    """
    data["bwhist"] = data.bwread.apply(lambda x: x * 8 / 2e9) + data.bwwrite.apply(lambda x: x * 8 / 2e9)
    data["flavour"] = data.isexit + data.isguard
    data = data[data["flavour"] == "tf"]
    ax.plot(data["date"], data["bwhist"], color=color, label="Exit Bandwidth")
    ax.set_xlabel("Date")
    ax.set_ylabel("Consumed Exits Bandwidth")
    ax.yaxis.set_major_formatter(format_bandwidth)


def summary_cbandwidth_middle(ax, data, color):
    """
    Consumed bandwidth (middle only relays).
    """
    data["bwhist"] = data.bwread.apply(lambda x: x * 8 / 2e9) + data.bwwrite.apply(lambda x: x * 8 / 2e9)
    data["flavour"] = data.isexit + data.isguard
    data = data[data["flavour"] == "ff"]
    ax.plot(data["date"], data["bwhist"], color=color, label="Middle Bandwidth")
    ax.set_xlabel("Date")
    ax.set_ylabel("Consumed Middle Relays Bandwidth")
    ax.yaxis.set_major_formatter(format_bandwidth)


def summary_relayusers(ax, data, color):
    """
    Relay users.
    """
    data = data[(data.country.isna()) & (data.transport.isna()) & (data.version.isna())]
    data = data[data.node == "relay"]
    ax.plot(data["date"], data["clients"], color=color, label="Relay Users")
    ax.set_xlabel("Date")
    ax.set_ylabel("Total relay users")
    ax.yaxis.set_major_formatter(format_users)


def summary_bridgeusers(ax, data, color):
    """
    Bridge users.
    """
    data = data[(data.country.isna()) & (data.transport.isna()) & (data.version.isna())]
    data = data[data.node == "bridge"]
    ax.plot(data["date"], data["clients"], color=color, label="Total Users")
    ax.set_xlabel("Date")
    ax.set_ylabel("Total bridge users")
    ax.yaxis.set_major_formatter(format_users)


def summary_totalusers(ax, data, color):
    """
    Total users.
    """
    data = data[(data.country.isna()) & (data.transport.isna()) & (data.version.isna())]
    data = data.groupby("date").sum()
    ax.plot(data.index, data["clients"], color=color, label="Users")
    ax.set_xlabel("Date")
    ax.set_ylabel("Total users across relays and bridges")
    ax.yaxis.set_major_formatter(format_users)


def summary_ttdl_5mb_public(ax, data, color):
    """
    Time to download 5MB (exit circuit).
    """
    data = data[(data.server == "public") & (data.filesize == 5242880)]
    data.md = data.md.apply(lambda x: x / 1000)
    data = data.groupby("date").sum()
    ax.plot(data.index, data.md, color=color, label="Time to download")
    ax.set_xlabel("Date")
    ax.set_ylabel("Time to download 5MB from public server")
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.1f s'))


def summary_ttdl_5mb_onion(ax, data, color):
    """
    Time to download 5MB (onion circuit).
    """
    data = data[(data.server == "onion") & (data.filesize == 5242880)]
    data.md = data.md.apply(lambda x: x / 1000)
    data = data.groupby("date").sum()
    ax.plot(data.index, data.md, color=color, label="Time to download")
    ax.set_xlabel("Date")
    ax.set_ylabel("Time to download 5MB from onion server")
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.1f s'))

def summary_onion_v3_size(ax, data, color):
    """
    Number of Onion addresses (v3).
    """
    data = data[(data.type == "dir-onions-seen") & (data.frac > 0.01)]
    ax.plot(data.date, data.wiqm, color=color, label="Number of addresses")
    ax.set_xlabel("Date")
    ax.set_ylabel("Number of unique .onion v3 addresses")
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))

def summary_onion_traffic(ax, data, color):
    """
    Onion traffic.
    """
    data = data[(data.type == "rend-relayed-cells") & (data.frac > 0.01)]
    data.wiqm = data.wiqm.apply(lambda x: x * 8 * 512 / (86400 * 1e9))
    ax.plot(data.date, data.wiqm, color=color, label="Gbit/s")
    ax.set_xlabel("Date")
    ax.set_ylabel("Onion service traffic")
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.1f Gbit/s'))
