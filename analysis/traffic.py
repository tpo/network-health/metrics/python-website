"""
Traffic.

We measure total available bandwidth and current capacity by aggregating what relays and bridges report to directory
authorities.
"""

from matplotlib.figure import Figure
import numpy as np
import seaborn as sns

from analysis.utils import prepare_file, add_missing_dates, set_labels

sns.set_theme(context="notebook")


def plot_bandwidth(start, end):
    """
    Total relay bandwidth.

    This graph shows the total advertised bandwidth and consumed bandwidth
    of all relays in the network.

    :param start: start date
    :param end: end date
    :return: the rendered plot
    """
    p1 = prepare_file(start, end, "advbw.csv")
    p1["advbw"] = p1.advbw.apply(lambda x: x * 8 / 1e9)

    p2 = prepare_file(start, end, "bandwidth.csv").dropna(subset=['isguard', 'isexit'])
    p2["bwhist"] = p2.bwread.apply(lambda x: x * 8 / 2e9) + p2.bwwrite.apply(lambda x: x * 8 / 2e9)

    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()
    p1 = p1.groupby("date").sum()
    p1["date"] = p1.index
    p1 = add_missing_dates(p1)
    p2 = p2.groupby("date").sum()
    p2["date"] = p2.index
    p2 = add_missing_dates(p2)
    ax.plot(p1.date, p1.advbw, label="Advertised bandwidth")
    ax.plot(p2.date, p2.bwhist, label="Bandwidth history")

    set_labels(ax, "Total relay bandwidth", "Date", "Gbit/s")
    ax.legend()
    return fig


def plot_bandwidth_flags(start, end):
    """
    Advertised and consumed bandwidth by relay flags.

    This graph shows advertised bandwidth and consumed bandwidth of relays
    with "Exit" and/or "Guard" flags assigned by the directory authorities.

    :param start: start date
    :param end: end date
    :return: the rendered plot
    """
    flavour = {"ff": "Neither Guard nor Exit",
               "ft": "Guard only",
               "tf": "Exit only",
               "tt": "Guard and Exit"}
    p1 = prepare_file(start, end, "advbw.csv")
    p1["advbw"] = p1.advbw.apply(lambda x: x * 8 / 1e9)
    p1["flavour"] = p1.isexit + p1.isguard
    p1["Advertised Bandwidth"] = p1.flavour.apply(lambda x: flavour[x])

    p2 = prepare_file(start, end, "bandwidth.csv").dropna(subset=['isguard', 'isexit'])
    p2["bwhist"] = p2.bwread.apply(lambda x: x * 8 / 2e9) + p2.bwwrite.apply(lambda x: x * 8 / 2e9)
    p2["flavour"] = p2.isexit + p2.isguard
    p2["Consumed Bandwidth"] = p2.flavour.apply(lambda x: flavour[x])

    fig = Figure(figsize=(12, 6))
    axes = fig.subplots(2, 1)
    axes[0] = sns.lineplot(data=p1, x="date", y="advbw", hue="Advertised Bandwidth", ax=axes[0])
    l = axes[0].get_ylim()
    axes[1] = sns.lineplot(data=p2, x="date", y="bwhist", hue="Consumed Bandwidth", ax=axes[1])
    axes[1].set_ylim(l)

    set_labels(axes[0], "Advertised and consumed bandwidth by relay flags", "", "Gb/s", add_copyright_notice=False)
    set_labels(axes[1], "", "Date", "Gb/s", push=-0.15)
    return fig


def plot_advbw_ipv6(start, end):
    """
    Advertised bandwidth by IP version.

    This graph shows total advertised bandwidth by relays supporting IPv6 as
    compared to all relays. A relay can support IPv6 by announcing an IPv6
    address and port for the OR protocol, which may then be confirmed as
    reachable by the directory authorities, and by permitting exiting to
    IPv6 targets. In some cases, relay sets are broken down by whether relays
    got the "Guard" and/or "Exit" relay flags indicating their special
    qualification for the first or last position in a circuit. These sets are
    not distinct, because relays can have various combinations of
    announced/confirmed OR ports, exit policies, and relay flags.

    :param start: start date
    :param end: end date
    :return: the rendered plot
    """
    p = prepare_file(start, end, "ipv6servers.csv")
    p = p[p["server"] == "relay"]
    p["advertised_bandwidth_bytes_sum_avg"] = p.advertised_bandwidth_bytes_sum_avg.apply(lambda x: x * 8 / 1e9)
    fig = Figure((12,6))
    ax = fig.subplots()
    sns.lineplot(data=p.groupby("valid_after_date").sum(),
                 x="valid_after_date",
                 y="advertised_bandwidth_bytes_sum_avg",
                 label="Total (IPv4) OR",
                 ax=ax)
    sns.lineplot(data=p[p["guard_relay"] == "t"].groupby("valid_after_date").sum(),
                 x="valid_after_date",
                 y="advertised_bandwidth_bytes_sum_avg",
                 label="Guard Total (IPv4)",
                 ax=ax)
    sns.lineplot(data=p[p["exit_relay"] == "t"].groupby("valid_after_date").sum(),
                 x="valid_after_date",
                 y="advertised_bandwidth_bytes_sum_avg",
                 label="Exit Total (IPv4)",
                 ax=ax)
    sns.lineplot(
        data=p[(p["exit_relay"] == "t") & (p["reachable_ipv6_relay"] == "t")].groupby("valid_after_date").sum(),
        x="valid_after_date",
        y="advertised_bandwidth_bytes_sum_avg",
        label="Reachable Exit IPv6 OR",
        ax=ax)
    sns.lineplot(
        data=p[(p["guard_relay"] == "t") & (p["reachable_ipv6_relay"] == "t")].groupby("valid_after_date").sum(),
        x="valid_after_date",
        y="advertised_bandwidth_bytes_sum_avg",
        label="Reachable Guard IPv6 OR",
        ax=ax)
    sns.lineplot(data=p[p["exiting_ipv6_relay"] == "t"].groupby("valid_after_date").sum(),
                 x="valid_after_date",
                 y="advertised_bandwidth_bytes_sum_avg",
                 label="IPv6 Exiting",
                 ax=ax)
    set_labels(ax, "Advertised bandwidth distribution by IP version", "Date", "Gbit/s")

    return fig


def plot_advbwdist_perc(start, end, percentiles=[25, 50, 75, 100]):
    """
    Advertised bandwidth distribution.

    This graph shows the distribution of the advertised bandwidth of relays
    in the network.  Each percentile represents the advertised bandwidth
    that a given percentage of relays does not exceed (and that in turn the
    remaining relays either match or exceed).  For example, 99% of relays
    advertise at most the bandwidth value shown in the 99th percentile line
    (and the remaining 1% advertise at least that amount).

    :param start: start date
    :param end: end date
    :param percentiles: list of percentiles to display on the plot
    :return: the rendered plot
    """
    p = prepare_file(start, end, "advbwdist.csv")
    p["advbw"] = p.advbw.apply(lambda x: x * 8 / 1e9)
    fig = Figure(figsize=(12, 6))
    axes = fig.subplots(2, 1)
    p = p[p.percentile.isin(percentiles)]
    p["percentile"] = p.percentile.apply(lambda x: str(int(x)))
    p1 = p.groupby(["date", "percentile"]).mean()
    p2 = p[p["isexit"] == "t"].groupby(["date", "percentile"]).mean()
    axes[0] = sns.lineplot(data=p1, x=p1.index.get_level_values(0), y="advbw", hue=p1.index.get_level_values(1),
                           ax=axes[0])
    axes[1] = sns.lineplot(data=p2, x=p2.index.get_level_values(0), y="advbw", hue=p2.index.get_level_values(1),
                           ax=axes[1])
    set_labels(axes[0], "Advertised bandwidth distribution", "", "All relays - Gbit/s", add_copyright_notice=False)
    set_labels(axes[1], "", "Date", "Exits only - Gbit/s", push=-0.15)
    # axes[0].legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0)
    # axes[1].legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0)

    return fig


def plot_dirbytes(start, end):
    """
    Bandwidth spent on answering directory requests.

    :param start: start date
    :param end: end date
    :return: the rendered plot
    """
    p = prepare_file(start, end, "bandwidth.csv")
    p["dirread"] = p.dirread.apply(lambda x: x * 8 / 1e9)
    p["dirwrite"] = p.dirwrite.apply(lambda x: x * 8 / 1e9)
    p["dirauthread"] = p.dirauthread.apply(lambda x: x * 8 / 1e9)
    p["dirauthwrite"] = p.dirauthwrite.apply(lambda x: x * 8 / 1e9)
    fig = Figure()
    ax = fig.subplots()
    p = p.groupby("date").sum().replace(0, np.nan)
    p["date"] = p.index
    p = add_missing_dates(p)
    ax.plot(p.date, p.dirread, label="Read dir bytes")
    ax.plot(p.date, p.dirwrite, label="Written dir bytes")
    set_labels(ax, "Number of bytes spent answering directory requests", "Date", "Gb/s")
    ax.legend()
    return fig
