from analysis.utils import prepare_file
from datetime import timedelta, datetime

def trends_relays_lastxdays(x=100):
    td = timedelta(x)
    p = prepare_file(datetime.now() - td, datetime.now(), "networksize.csv")
    return int(p.relays.mean())
    """
    Number of relays in the last x days.
    """

def trends_users_lastxdays(x=30):
    """
    Number of users in the last 30 days.
    """
    td = timedelta(x)
    p = prepare_file(datetime.now() - td, datetime.now(), "clients.csv")
    p = p[(p.country.isna()) & (p.transport.isna()) & (p.version.isna())].copy().reset_index()
    p = p.groupby("date").sum()
    return int(p.clients.mean())


def trends_advbandwidth_lastxdays(x=30):
    """
    Advertised bandwidth in the last 30 days.
    """
    td = timedelta(x)
    p = prepare_file(datetime.now() - td, datetime.now(), "advbw.csv")
    p["advbw"] = p.advbw.apply(lambda x: x*8/1e9)
    p = p.groupby("date").sum()
    return f"{p.advbw.mean():3.1f} Gbit/s"


def trends_cbandwidth_lastxdays(x=30):
    """
    Consumed bandwidth in the last x days.
    """
    td = timedelta(x)
    p = prepare_file(datetime.now() - td, datetime.now(), "bandwidth.csv").dropna(subset = ['isguard', 'isexit'])
    p["bwhist"] = p.bwread.apply(lambda x: x*8/2e9) + p.bwwrite.apply(lambda x: x*8/2e9)
    p = p.groupby("date").sum()
    return f"{p.bwhist.mean():3.1f} Gbit/s"
    pass