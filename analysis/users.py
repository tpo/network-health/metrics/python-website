"""
Users.

We estimate the number of users by analyzing the requests induced by clients to relays and bridges.
"""

from matplotlib.figure import Figure
import pandas as pd
import seaborn as sns

from analysis.utils import prepare_file, add_missing_dates, set_labels

sns.set_theme(context="notebook")


def plot_userstat_relay_bridge_country(start, end, countries, servers):
    """
    Total users by country.

    This graph shows the number of users using either relays and/or bridges by country.

    :param start:
    :param end:
    :param countries:
    :param servers:
    :return:
    """
    p = prepare_file(start, end, "clients.csv")
    # filter countries to keep
    df_countries = p[p.country.isin(countries)].copy()
    if "all" in countries:
        df_all = p[(p.country.isna()) & (p.transport.isna()) & (p.version.isna())].copy().reset_index()
        df_all["country"] = 'all'
        counts = p.groupby(["date", "node"]).sum().reset_index()
        df_all["upper"] = counts.upper
        df_all["lower"] = counts.lower
        df_countries = pd.concat([df_all, df_countries])
    # make a new df keeping the servers we're interested in.
    df_countries_servers = df_countries[df_countries.node.isin(servers)].copy()
    # create a new column combining the country and node name
    df_countries_servers['country_node'] = df_countries_servers['country'] + "-" + df_countries_servers['node']
    df_countries["country_node"] = df_countries["country"] + "-total"
    if "total" in servers:
        df_countries_servers = pd.concat([df_countries_servers, df_countries])

    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()

    for c in countries:
        for r in servers:
            df = df_countries_servers[df_countries_servers.country_node == f"{c}-{r}"]
            if r == "total":
                count_groups = df.groupby(["date"]).size()
                index_list = count_groups[count_groups > 1].index.to_list()
                df = df[df.date.isin(index_list)]

            df = df.groupby(["date"]).sum()
            df["date"] = df.index
            df = add_missing_dates(df)
            plot = ax.plot(df.date, df.clients, label=f"{c}-{r}")
            # ax.fill_between( df.date, df.clients, df.upper, color= plot[-1].get_color(),alpha=0.3)
            # ax.fill_between( df.date, df.lower, df.clients, color= plot[-1].get_color(),alpha=0.3)

    set_labels(ax, "Number of users", "Date", "Total")
    ax.legend()
    return fig


def plot_userstats_bridge_combined(start, end, countries, transports):
    """
     Bridge users by country and transport.

     This graph shows the estimated number of clients connecting via bridges.
     These numbers are derived from directory requests counted on bridges.
     Bridges resolve client IP addresses of incoming directory requests to country codes,
     and they distinguish connecting clients by transport protocol, which may include pluggable transports.
     Even though bridges don't report a combination of clients by country and transport,
     it's possible to derive and graph lower and upper bounds from existing usage statistics.

     :param start:
     :param end:
     :param countries:
     :param transports:
     :return:
     """
    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()

    if "all" in countries:
        p = prepare_file(start, end, "clients.csv")
        df_all = p[(p.country.isna()) &
                   (p.transport.notna()) &
                   (p.version.isna()) &
                   (p.node == "bridge")].copy()
        for transport in transports:
            df_plot = df_all[df_all.transport == transport]
            if not df_plot.empty:
                df_plot = add_missing_dates(df_plot)
                ax.plot(df_plot.date, df_plot.clients, label=f"all -{transport}")
        countries.remove("all")
    if countries:
        p = prepare_file(start, end, "userstats-combined.csv")
        df_all = p.groupby(["date", "country", "transport"]).sum()
        for country in countries:
            df_c = df_all[df_all.index.get_level_values(1) == country]
            for transport in transports:
                df_plot = df_c[df_c.index.get_level_values(2) == transport].copy()
                df_plot["date"] = df_plot.index.get_level_values(0)
                if not df_plot.empty:
                    df_plot = add_missing_dates(df_plot)
                    plot = ax.plot(df_plot.date, df_plot.low, label=f"{country}-{transport}")
                    ax.plot(df_plot.date, df_plot.high, color=plot[-1].get_color())
                    ax.fill_between(df_plot.date, df_plot.low, df_plot.high, alpha=0.8, color=plot[-1].get_color())
    ax.legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0)

    set_labels(ax, "Bridge users by transport", "Date", "Total clients")
    return fig
