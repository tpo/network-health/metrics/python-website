import io
from typing import List, Dict

import numpy as np
import pandas as pd


def analysis_title(func) -> str:
    print(func)
    for line in func.__doc__.split("\n"):
        line = line.strip()
        if len(line) > 0:
            return line.strip().strip(".")


def analysis_description(func) -> str:
    skip_title = True
    description = ""
    for line in func.__doc__.split("\n"):
        line = line.strip()
        if skip_title:
            if len(line) > 0:
                skip_title = False
                continue
        if len(line) > 0 and line[0] == ":":
            break
        description += line + " "
    return description.strip()


def supported_parameters(func) -> List[str]:
    return func.__code__.co_varnames[:func.__code__.co_argcount][::-1]


def encode_image(fig, form='png'):
    buffer = io.BytesIO()
    fig.savefig(buffer, dpi=150, format=form)
    buffer.seek(0)  # rewind the data
    return buffer.getvalue()


def add_missing_dates(df):
    # assumes missing dates are in a col called date
    date_range = pd.date_range(start=df.date.min(), end=df.date.max())
    df = df.set_index('date').reindex(date_range).fillna(np.nan).rename_axis('date').reset_index()
    return df

def add_missing_countries(df):
    #assumes missing dates are in a col called alpha_3
    cdf = pd.read_csv("analysis/data/countries.csv")
    all_countries = cdf["alpha-3"]
    df = df.set_index('iso_a3').reindex(all_countries).fillna(np.nan).rename_axis('iso_a3').reset_index()
    return df

def prepare_file(start, end, csv_name):
    df = read_in_file(csv_name)
    prep = df[(df.date >= start) & (df.date <= end)]
    return prep


def set_labels(axis, title, xlab, ylab, add_copyright_notice=True, push=0):
    axis.set_title(title)
    axis.set_xlabel(xlab)
    axis.set_ylabel(ylab)
    if add_copyright_notice:
        copyright_text = "The Tor Project - https://metrics.torproject.org/"
        axis.text(0.8, -0.1 + push, copyright_text, wrap=True, horizontalalignment='center', transform=axis.transAxes)
    # plt.tight_layout()
    return axis


def countryname(countrycode):
    if countrycode in countries:
        return countries[countrycode]
    else:
        return "no man's land"


def format_users(x, pos=None):
    x = float(x)/100000.0
    return f"{x:3.1f} M"


def format_bandwidth(x, pos=None):
    return f"{x:3.1f} Gbit/s"


def custom_breaks(d_input):
    pass


def custom_minor_breaks(d_input):
    pass


def custom_labels(breaks):
    pass


def formatter(x):
    pass


def read_in_file(csv_name):
    stats_dir = 'shared/stats/'
    df = pd.read_csv(stats_dir + csv_name)
    if 'date' in df.columns:
        df.date = pd.to_datetime(df.date)
    if 'valid_after_date' in df.columns:
        df["valid_after_date"] = df["date"] = pd.to_datetime(df.valid_after_date)
    return df
