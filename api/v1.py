from flask import Blueprint
from flask import Response

from analysis import plots
from analysis.utils import encode_image
from request_utils import request_format, request_filters
from utils import matplotlib_formats

api = Blueprint('api_v1', __name__)


@api.route('/plot/<plot_name>', methods=['GET'])
def api_plot(plot_name):
    fmt = request_format()
    filters = request_filters(plot_name)
    data = encode_image(
        plots[plot_name](**filters), fmt
    )
    return Response(data, mimetype=matplotlib_formats[fmt])
