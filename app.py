import datetime
import urllib.parse
import json
from typing import Dict, List, Any, Optional

import jinja2
import markupsafe
from docutils.core import publish_parts
from flask import Flask
from flask import render_template

from analysis import plots, categories
from analysis.summary import plotters as summary_plotters
from analysis.summary import trends as summary_trends

from analysis.filters import choices
from api.v1 import api
from rs import relay_search
from analysis.utils import analysis_title, analysis_description, supported_parameters
from request_utils import request_filters
from utils import parse_date

app = Flask(__name__)


@app.template_filter("analysis_title")
def analysis_title_filter(s):
    try:
        return jinja2.Markup(analysis_title(plots[s]))
    except KeyError:
        return "missing"


@app.template_filter("summary_title")
def analysis_title_filter(s):
    try:
        return jinja2.Markup(analysis_title(summary_plotters[s]))
    except KeyError:
        return "missing"

@app.template_filter("rst")
def rst_filter(s):
    return markupsafe.Markup(publish_parts(source=s, writer_name='html')['body'])


app.register_blueprint(api, url_prefix="/api/v1")
app.register_blueprint(relay_search, url_prefix="/rs")


@app.route('/')
def hello_world():
    filters = request_filters("summary")
    trends = dict()
    print(summary_trends)
    for trend in summary_trends:
        trends[trend] = summary_trends[trend](x=100)
    print(trends)
    return render_template("home.html.j2",
                           categories=categories,
                           encoded=encode_filters("summary", filters),
                           filters=filters,
                           summary_plotters=summary_plotters,
                           trends=trends,
                           news=filtered_news({
                               "start": datetime.datetime.now() - datetime.timedelta(days=120),
                               "end": datetime.datetime.now()
                           })[:5])


@app.route('/index')
def index():
    return render_template("dashboards.html.j2",
                           categories=categories)


@app.route('/page/<content>')
def page(content):
    if ".." in content:
        return 400, "Nice try"
    with open("pages/" + content + ".html") as p:
        html = p.read()
    return render_template("page.html.j2",
                           categories=categories,
                           html=html)


@app.route('/news')
def news():
    return render_template("news.html.j2",
                           categories=categories,
                           news=filtered_news({}))

@app.route('/health', methods=['post', 'get'])
def health():
    display_plots = ["networksize", "bandwidth"]
    d_plots = dict()
    for plot in display_plots:
        d_plots[plot] = dict()
        plot_func = plots[plot]
        if plot_func is None:
            return "Could not find plot", 404
        d_plots[plot]["title"] = analysis_title(plot_func)
        d_plots[plot]["description"] = analysis_description(plot_func)
        d_plots[plot]["parameters"] = supported_parameters(plot_func)

    return render_template("health.html.j2",
                           plots=d_plots,
                           categories=categories)


def news_date_relevant(request_start: datetime.datetime, request_end: datetime.datetime,
                       news_item: Dict[str, Any]) -> bool:
    news_start: Optional[datetime.datetime] = parse_date(news_item["start"]) if "start" in news_item else None
    news_end: Optional[datetime.datetime] = parse_date(news_item["end"]) if "end" in news_item else news_start
    news_ongoing: bool = news_item["ongoing"] if "ongoing" in news_item else False
    print(news_start, news_end,request_start,request_end)
    if news_ongoing and news_start <= request_end:
        return True
    if news_end <= request_start or news_start >= request_end:
        return False
    return True


def filtered_news(filters) -> List[Dict[str, Any]]:
    desired_news = []
    with open("shared/news.json") as news_f:
        news = json.load(news_f)
        for news_item in news:
            if "start" in filters and not news_date_relevant(filters['start'], filters['end'], news_item):
                continue
            desired_news.append(news_item)
    return desired_news


def encode_filters(plot, filters) -> str:
    args = dict()
    parameters = supported_parameters(plots[plot])
    if "start" in parameters:
        args["start"] = filters["start"].strftime("%Y-%m-%d")
    if "end" in parameters:
        args["end"] = filters["end"].strftime("%Y-%m-%d")
    if "countries" in parameters:
        args["countries"] = ",".join(filters["countries"])
    if "servers" in parameters:
        args["servers"] = ",".join(filters["servers"])
    if "transports" in parameters:
        args["transports"] = ",".join(filters["transports"])
    if "filesize" in parameters:
        args["filesize"] = filters["filesize"]
    if "percentiles" in parameters:
        args["percentiles"] = ",".join([str(x) for x in filters["percentiles"]])
    if "dataset1" in parameters:
        args["dataset1"] = filters["dataset1"]
    if "dataset2" in parameters:
        args["dataset2"] = filters["dataset2"]
    if "webserver" in parameters:
        args["webserver"] = filters["webserver"]
    if "relay_type" in parameters:
        args["relay_type"] = filters["relay_type"]
    return urllib.parse.urlencode(args)


@app.route('/plot/<plot>', methods=['GET', 'POST'])
def web_plot(plot):
    plot_func = plots[plot]
    if plot_func is None:
        return "Could not find plot", 404
    title = analysis_title(plot_func)
    description = analysis_description(plot_func)
    parameters = supported_parameters(plot_func)
    filters = request_filters(plot)

    return render_template("graphs.html.j2",
                           plot=plot,
                           title=title,
                           description=description,
                           parameters=parameters,
                           encoded=encode_filters(plot, filters),
                           filters=filters,
                           categories=categories,
                           choices=choices,
                           news=filtered_news(filters))


if __name__ == '__main__':
    app.run()
