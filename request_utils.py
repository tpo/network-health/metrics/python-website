from datetime import datetime, timedelta
from typing import List, Dict

from flask import request

from analysis import plots
from analysis.utils import supported_parameters
from utils import parse_date
from utils import matplotlib_formats
from utils import mimetype_to_format
from analysis.filters import choices


def request_format() -> str:
    fmt = request.args.get('format')
    if fmt is not None:
        if fmt in matplotlib_formats.keys():
            return fmt
        else:
            # TODO: This should raise some exception
            return "png"
    best = request.accept_mimetypes.best_match(matplotlib_formats.values())
    if best is not None:
        return mimetype_to_format(best)
    return "png"


def request_start_date() -> datetime:
    start_date = datetime.now() - timedelta(days=100)
    start_date_str = request.args.get("start")
    if start_date_str is not None:
        start_date = parse_date(start_date_str)
    return start_date


def request_end_date() -> datetime:
    # max_date: The most recent values can turn out to be misleading
    # as new information filters in. The two day buffer gives us some
    # reassurance that we've got some representative data, not wildly
    # extrapolated from unrepresentative sample sizes.
    max_date = datetime.today() - timedelta(days=2)
    end_date_str = request.args.get("end")
    if end_date_str is not None:
        end_date = parse_date(end_date_str)
        return min(end_date, max_date)
    return max_date


def request_countries() -> List[str]:
    rlist = request.args.getlist("countries")
    if not rlist:
        return ["all"]
    result = []
    for countries_str in rlist:
        result.extend(countries_str.split(","))
    return result

def request_transports() -> List[str]:
    rlist = request.args.getlist("transports")
    if not rlist:
        return choices["transports"].keys()
    result = []
    for trans_str in rlist:
        result.extend(trans_str.split(","))
    return result

def request_webserver() -> str:
    webserver = request.args.get("webserver")
    if not webserver:
        return "public"
    return webserver

def request_filesize() -> int:
    filesize = request.args.get("filesize")
    if not filesize:
        return 51200
    return filesize

def request_servers() -> List[str]:
    rlist = request.args.getlist("servers")
    if not rlist:
        return ["total"]
    result = []
    for servers_str in rlist:
        result.extend(servers_str.split(","))
    return result

def request_percentiles() -> List[str]:
    rlist = request.args.getlist("percentiles")
    if not rlist:
        return ["100", "75", "50", "25"]
    result = []
    for percentiles_str in rlist:
        result.extend([int(x) for x in percentiles_str.split(",")])
    return result


def request_filters(plot) -> Dict[str, List[str]]:
    filters = dict()
    parameters = supported_parameters(plots[plot])
    if "start" in parameters:
        filters["start"] = request_start_date()
    if "end" in parameters:
        filters["end"] = request_end_date()
    if "countries" in parameters:
        filters["countries"] = request_countries()
    if "transports" in parameters:
        filters["transports"] = request_transports()
    if "webserver" in parameters:
        filters["webserver"] = request_webserver()
    if "servers" in parameters:
        filters["servers"] = request_servers()
    if "percentiles" in parameters:
        filters["percentiles"] = request_percentiles()
    if "filesize" in parameters:
        filters["filesize"] = request_filesize()
    if "dataset1" in parameters:
        filters["dataset1"] = request.args.get("dataset1", "relaysize")
    if "dataset2" in parameters:
        filters["dataset2"] = request.args.get("dataset2", "relayusers")
    if "relay_type" in parameters:
        filters["relay_type"] = request.args.get("relay_type", "relay")
    return filters
