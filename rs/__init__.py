from typing import Optional

import markupsafe
import requests
from flask import Blueprint
from flask import render_template
from flask import request

relay_search = Blueprint('relay_search', __name__, template_folder="templates")

def sizeof_fmt(num, suffix="B"):
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"


@relay_search.app_template_filter("human_Bps")
def human_bytes_per_second_filter(s):
    return markupsafe.Markup(sizeof_fmt(s, "Bps"))


@relay_search.route('/', methods=['GET'])
def rs_search():
    query = request.args.get("query", None)
    if query is None or query.strip() == "":
        return render_template("search.html.j2")
    elif "overload:true" in query:
        new_query = query.replace("overload:true", "").strip()
        if len(new_query) > 0:
            query_url = "https://onionoo.torproject.org/details?search=" + new_query
        else:
            query_url = "https://onionoo.torproject.org/details"
        r = requests.get(query_url)
        results = r.json()
        results["relays"] = [x for x in results["relays"] if x.get("overload_general_timestamp", False)]
        results["bridges"] = [x for x in results["bridges"] if x.get("overload_general_timestamp", False)]
    else:
        r = requests.get("https://onionoo.torproject.org/details?search=" + query)
        results = r.json()
    for i in range(len(results['relays'])):
        results['relays'][i]['dir_port'] = results['relays'][i].get('dir_address', ":None").split(":")[1]
        for or_address in results['relays'][i]['or_addresses']:
            if "." in or_address:
                results['relays'][i]['or_v4_address'] = results['relays'][i].get('or_v4_address', None) or or_address
            else:
                results['relays'][i]['or_v6_address'] = results['relays'][i].get('or_v6_address', None) or or_address
            if results['relays'][i].get('or_v4_address', False) and results['relays'][i].get('or_v6_address', False):
                break
    return render_template("results.html.j2", query=query, results=results)

@relay_search.route("/details/<fingerprint>", methods=['GET'])
def rs_details(fingerprint):
    r = requests.get("https://onionoo.torproject.org/details?lookup=" + fingerprint)
    result = r.json()
    contact = None
    if "ciissversion:" in result["relays"][0]["contact"]:
        contact = {}
        freetext = []
        for part in result["relays"][0]["contact"].split(" "):
            piece = part.split(":", 1)
            if len(piece) == 2:
                contact[piece[0]] = piece[1]
            else:
                freetext.append(part)
        contact["freetext"] = " ".join(freetext)
    return render_template("details.html.j2", server=result["relays"][0], contact=contact)