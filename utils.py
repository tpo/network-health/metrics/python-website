from collections import OrderedDict
from datetime import datetime
from typing import Optional

matplotlib_formats = OrderedDict()
matplotlib_formats["png"] = "image/png"
matplotlib_formats["jpg"] = "image/jpeg"
matplotlib_formats["tiff"] = "image/tiff"
matplotlib_formats["svg"] = "image/svg"
matplotlib_formats["pdf"] = "application/pdf"
matplotlib_formats["ps"] = "application/postscript"
matplotlib_formats["pgf"] = "application/x-latex"


def format_to_mimetype(fmt: str) -> str:
    return matplotlib_formats.get(fmt)


def mimetype_to_format(mimetype: str) -> Optional[str]:
    for k, v in matplotlib_formats.items():
        if v == mimetype:
            return k
    return None


def parse_date(date_str: str) -> datetime:
    return datetime.strptime(date_str, "%Y-%m-%d")
